from django.contrib.auth.forms import UserCreationForm
from django.db.transaction import atomic
from django.forms import CharField, DateField, EmailField

from accounts.models import UserProfile, AppUser


class SignUpForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = AppUser
        fields = ["username", "first_name", "last_name", "password1", "password2"]

    phone_number = CharField(max_length=32, required=True)
    email = EmailField(max_length=128, required=True)
    date_of_birth = DateField(required=False)

    @atomic
    def save(self, commit=True):
        result = super().save(commit)
        profile = UserProfile(
            user=result,
            phone_number=self.cleaned_data["phone_number"],
            email=self.cleaned_data["email"],
            date_of_birth=self.cleaned_data["date_of_birth"],
        )

        if commit:
            profile.save()

        return result
