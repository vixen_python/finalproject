from datetime import datetime

from django.contrib.auth.models import AbstractUser
from django.db.models import (
    CASCADE,
    CharField,
    DateField,
    EmailField,
    Model,
    OneToOneField,
)


class AppUser(AbstractUser):
    class Meta:
        db_table = "auth_user"
        app_label = "accounts"

    @property
    def active_registrations(self):
        current_dt = datetime.today()
        current_date = current_dt.date()

        return self.participation.filter(
            schedule_event__date__gte=current_date
        )


class UserProfile(Model):
    user = OneToOneField(AppUser, on_delete=CASCADE, related_name="profile")
    phone_number = CharField(max_length=32, default="", null=False, blank=False)
    email = EmailField(max_length=128, default="", null=False, blank=False)
    date_of_birth = DateField(null=True, blank=True)
