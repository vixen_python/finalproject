from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path

from accounts.views import SignUpView, SubmittableLogoutView, UserProfileView

app_name = "accounts"

urlpatterns = [
    path("", UserProfileView.as_view(), name="user_profile"),
    path("sign_up/", SignUpView.as_view(), name="sign_up"),
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("logout/request/", SubmittableLogoutView.as_view(), name="logout_request"),
]
