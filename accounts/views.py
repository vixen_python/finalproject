from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, TemplateView

from accounts.forms import SignUpForm
from accounts.models import UserProfile


class UserProfileView(ListView):
    template_name = "user_card.html"
    model = UserProfile


class SignUpView(CreateView):
    template_name = "form.html"
    form_class = SignUpForm
    success_url = reverse_lazy("index")


class SubmittableLogoutView(TemplateView):
    template_name = "registration/logged_out.html"
