from django.contrib import admin
from django.contrib.admin import ModelAdmin

from scheduler.models import Event, Exercise, Lector, Participation, Schedule


class ExerciseAdmin(ModelAdmin):
    fieldsets = [
        ("Exercise", {"fields": ["name", "description"]}),
    ]


class LectorAdmin(ModelAdmin):
    fieldsets = [
        ("Lector", {"fields": ["first_name", "last_name"]}),
    ]


class EventAdmin(ModelAdmin):
    fieldsets = [
        (
            "Event",
            {
                "fields": [
                    "capacity",
                    "duration",
                    "difficulty_level",
                    "price",
                    "lector",
                    "exercise",
                ]
            },
        ),
    ]


class ScheduleAdmin(ModelAdmin):
    fieldsets = [
        ("Schedule", {"fields": ["date", "time", "event", "gym_hall"]}),
    ]


class ParticipationAdmin(ModelAdmin):
    fieldsets = [
        (
            "Participation",
            {"fields": ["schedule_event", "participant", "created", "canceled"]},
        ),
    ]


admin.site.register(Exercise, ExerciseAdmin)
admin.site.register(Lector, LectorAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(Schedule, ScheduleAdmin)
admin.site.register(Participation, ParticipationAdmin)
