from scheduler.enums import Difficulty, Hall

DIFFICULTY_TYPES = (
    (Difficulty.beginner.value, Difficulty.beginner.name),
    (Difficulty.intermediate.value, Difficulty.intermediate.name),
    (Difficulty.advanced.value, Difficulty.advanced.name),
)

HALL_TYPES = (
    (Hall.mirror_room.value, Hall.mirror_room.name),
    (Hall.inside_hall.value, Hall.inside_hall.name),
    (Hall.outside.value, Hall.outside.name),
)
