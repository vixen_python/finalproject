from enum import IntEnum


class Difficulty(IntEnum):
    beginner = 10
    intermediate = 20
    advanced = 30


class Hall(IntEnum):
    mirror_room = 10
    inside_hall = 20
    outside = 30
