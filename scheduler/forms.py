from django.db.transaction import atomic
from django.forms import ModelForm

from scheduler.models import Participation


class RegistrationForm(ModelForm):
    class Meta:
        model = Participation
        fields = "__all__"

    @atomic
    def save(self, commit=True):
        result = super().save(commit)
        participation = Participation(
            schedule_event=result.schedule_event,
            participant=result.participant,
            created=result.created,
            canceled=result.canceled,
        )
        if commit:
            participation.save()

        return result
