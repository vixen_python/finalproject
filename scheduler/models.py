import datetime

from django.db.models import (
    CASCADE,
    PROTECT,
    CharField,
    DateField,
    DateTimeField,
    ForeignKey,
    IntegerField,
    Model,
    SmallIntegerField,
    TextField,
    TimeField,
)
from django.urls import reverse

from accounts.models import AppUser, UserProfile
from scheduler.choices import DIFFICULTY_TYPES, HALL_TYPES
from scheduler.enums import Hall


class Exercise(Model):
    name = CharField(max_length=64, null=False, blank=False)
    description = TextField(default="", null=False, blank=True)

    def __str__(self):
        return f"{self.name} ({self.id})"

    @property
    def image_name(self):
        return f"static_img/{self.name.lower()}.png"


class Lector(Model):
    first_name = CharField(max_length=64, null=False, blank=False)
    last_name = CharField(max_length=64, null=False, blank=False)

    @property
    def fullname(self):
        return f"{self.first_name} {self.last_name}"

    def __str__(self):
        return f"{self.fullname} ({self.id})"


class Event(Model):
    capacity = IntegerField(null=False, blank=False)
    duration = IntegerField(null=False, blank=False)
    difficulty_level = SmallIntegerField(
        choices=DIFFICULTY_TYPES, null=False, blank=False
    )
    price = IntegerField(null=False, blank=False)
    lector = ForeignKey(
        Lector, on_delete=PROTECT, null=False, related_name="events", default=1
    )
    exercise = ForeignKey(
        Exercise, on_delete=PROTECT, null=False, related_name="events", default=1
    )

    def __str__(self):
        return f"{self.exercise} - {self.difficulty_level} ({self.id})"

    @property
    def active_schedules(self):
        current_dt = datetime.datetime.today()
        return self.schedule.filter(date__gte=current_dt.date())


class Schedule(Model):
    class Meta:
        unique_together = ("date", "time", "event", "gym_hall")

    date = DateField(null=False, blank=False)
    time = TimeField(null=False, blank=False)
    event = ForeignKey(Event, null=False, on_delete=PROTECT, related_name="schedule")
    gym_hall = SmallIntegerField(choices=HALL_TYPES, null=False, blank=False)

    def __str__(self):
        return f"{self.event} - {self.gym_hall} ({self.id})"

    @property
    def capacity_filled(self):
        return len(self.participations.all())

    @property
    def day_of_week(self):
        return self.date.strftime("%A")


class Participation(Model):
    class Meta:
        unique_together = ("schedule_event", "participant")

    schedule_event = ForeignKey(
        Schedule, null=False, related_name="participations", on_delete=PROTECT
    )
    participant = ForeignKey(
        AppUser, null=False, related_name="participation", on_delete=CASCADE
    )
    created = DateTimeField(auto_now=True)

    @property
    def cancel_url(self) -> str:
        return reverse(
            "scheduler:cancel",
            kwargs={
                "room_type": Hall(self.schedule_event.gym_hall).name,
                "requested_date": self.schedule_event.date,
                "requested_time": self.schedule_event.time,
            },
        )
