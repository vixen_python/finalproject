import datetime

from celery import Celery
from celery.schedules import crontab

from scheduler.models import Schedule

app = Celery()


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(fill_scheduler, crontab(day_of_week=1 - 7))


@app.task
def fill_scheduler():
    currents = Schedule.objects.filter(date__exact=datetime.date.today())
    for current in currents:
        Schedule.objects.create(
            date=current.date + datetime.timedelta(days=7),
            time=current.time,
            event=current.event,
            gym_hall=current.gym_hall,
        )
