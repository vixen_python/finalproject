from django import template

from scheduler.enums import Difficulty, Hall

register = template.Library()


@register.filter
def hall_enum(value: int) -> str:
    return Hall(value).name


@register.filter
def difficulty_enum(value: int) -> str:
    return Difficulty(value).name
