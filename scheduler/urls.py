from django.urls import path

from scheduler.views import HallSelection, SchedulerView, cancel_registration, register

app_name = "scheduler"

urlpatterns = [
    path("", HallSelection.as_view(), name="hall_selection"),
    path("<str:room_type>/", SchedulerView.as_view(), name="table"),
    path(
        "<str:room_type>/register/<slug:requested_date>/<str:requested_time>/",
        register,
        name="register",
    ),
    path(
        "<str:room_type>/register/<slug:requested_date>/<str:requested_time>/cancel/",
        cancel_registration,
        name="cancel",
    ),
]
