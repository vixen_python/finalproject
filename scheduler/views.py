import json
from datetime import datetime, time, timedelta

from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.urls import reverse, reverse_lazy
from django.views import View
from django.views.generic import CreateView, ListView, TemplateView

from scheduler.enums import Hall
from scheduler.forms import RegistrationForm
from scheduler.models import Exercise, Participation, Schedule


class IndexView(ListView):
    template_name = "index.html"
    model = Exercise


class HallSelection(TemplateView, LoginRequiredMixin):
    template_name = "hall_selection.html"


ROOM_MAPPING = {
    "mirror_room": Hall.mirror_room,
    "inside_hall": Hall.inside_hall,
    "outside": Hall.outside,
}


class SchedulerView(TemplateView, LoginRequiredMixin):
    template_name = "schedule.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["room_type"] = kwargs["room_type"]

        room_type = ROOM_MAPPING.get(kwargs["room_type"])
        if not room_type:
            # TODO: throw exception and show it to user
            return {}

        first_day = datetime.today().date()
        last_day = first_day + timedelta(days=7)
        current_day = datetime.today().date()

        header = []
        for _ in range(7):
            header.append(
                {"name": current_day.strftime("%a %d.%m"), "date": current_day}
            )
            current_day += timedelta(days=1)

        start_hour = 6
        end_hour = 22

        schedule_map = {}
        planned_events = Schedule.objects.filter(
            gym_hall=room_type,
            date__gte=first_day,
            date__lte=last_day,
        )

        for event in planned_events:
            schedule_map[(event.date, event.time)] = event

        rows = []
        for hour in range(start_hour, end_hour + 1):
            days = []

            current_time = time(hour=hour, minute=0)
            for current_date in header:
                day = {
                    "lector": None,
                    "event": None,
                    "schedule": None,
                    "participate": True,
                    "capacity": None,
                    "date": current_date["date"],
                }

                planned_schedule = schedule_map.get(
                    (current_date["date"], current_time)
                )
                if planned_schedule:
                    day["schedule"] = planned_schedule
                    day["event"] = planned_schedule.event
                    day["participate"] = planned_schedule.participations.filter(
                        participant=self.request.user
                    ).exists()

                days.append(day)

            rows.append(
                {
                    "hour": time(hour=hour, minute=0).strftime("%H:%M"),
                    "days": days,
                }
            )

        context.update({"header": header, "rows": rows})

        return context


class LoginUserView(View):
    def post(self, request):
        output = {"message": ""}
        msg_success = "You are logged in"
        msg_fail = "I do not know you"

        request_json = json.loads(request.body)
        username = request_json.get("username", "").lower().strip()
        password = request_json.get("password")

        if username and password:
            if authenticate(username=username, password=password):
                output["message"] = msg_success
            else:
                output["message"] = msg_fail
        else:
            output["message"] = msg_fail

        return JsonResponse(output)


class RegisterView(CreateView):
    template_name = "registration.html"
    form_class = RegistrationForm
    success_url = reverse_lazy("scheduler:register")


@login_required
def register(request, room_type, requested_date, requested_time):
    # TODO: throw exception and show it to user
    room_type = ROOM_MAPPING.get(room_type)
    schedule_obj = Schedule.objects.filter(
        gym_hall=room_type, date=requested_date, time=requested_time
    ).first()

    if request.method == "GET":
        registered = Participation.objects.filter(
            participant_id=request.user.id, schedule_event_id=schedule_obj.id
        ).exists()

        return render(
            request,
            template_name="registration.html",
            context={
                "schedule": schedule_obj,
                "registered": registered,
                "cancel_url": reverse(
                    "scheduler:cancel",
                    kwargs={
                        "room_type": room_type.name,
                        "requested_date": requested_date,
                        "requested_time": requested_time,
                    },
                ),
            },
        )
    elif request.method == "POST":
        Participation.objects.create(
            schedule_event=schedule_obj,
            participant=request.user,
        )

    messages.success(request, "You have successfully signed up for class")

    return redirect(
        "scheduler:register",
        room_type=room_type.name,
        requested_date=requested_date,
        requested_time=requested_time,
    )


@login_required
def cancel_registration(request, room_type, requested_date, requested_time):
    # TODO: throw exception and show it to user
    room_type = ROOM_MAPPING.get(room_type)
    schedule_obj = Schedule.objects.filter(
        gym_hall=room_type, date=requested_date, time=requested_time
    ).first()

    if request.method == "POST":
        Participation.objects.filter(
            participant_id=request.user.id, schedule_event_id=schedule_obj.id
        ).delete()

    messages.success(request, "You have logged off from class")

    return redirect(
        "scheduler:register",
        room_type=room_type.name,
        requested_date=requested_date,
        requested_time=requested_time,
    )


@login_required
def schedule(request):
    return render(
        request,
        template_name="schedule.html",
        context={"schedule": Schedule.objects.all()},
    )


def exercise(request, exercise_type):
    return render(
        request,
        template_name="exercise.html",
        context={"exercise": Exercise.objects.filter(name=exercise_type).first()},
    )
