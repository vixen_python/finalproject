var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
})


function createScheduleTable() {
    $(document).ready(function () {
        $('#id-tab-schedule').DataTable(
            {
                "paging": false,
                "searching": false,
                "ordering": false,
                "info": false,
                "lengthChange": false
}
        );
        $("td#schedule-cell").click(
            function (e) {
                e.preventDefault();
                window.location.replace($(this).data('data-link'));
            }
        );
    });
}

createScheduleTable()